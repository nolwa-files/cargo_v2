<?php 
use \Milon\Barcode\DNS1D;
$d = new DNS1D();
?>

@php
                        $code = filter_var($shipment->code, FILTER_SANITIZE_NUMBER_INT);
                    @endphp
@extends('backend.layouts.app')

@section('content')

<style media="print">
    .no-print, div#kt_header_mobile, div#kt_header, div#kt_footer{
        display: none;
    }
    .brand-logo{
        text-align: center !important;
    }
    .company-name{
            margin-top: -10px;
    }
    .b-logo{
        height: "80px";
    }
    .padding010 {
    padding: 10px 0 !important;
}
@media print {
    @page {
        margin-top: 0;
        margin-bottom: 0;
    }
    body {
        padding-top: 72px;
        padding-bottom: 72px ;
    }
}
</style>
<!--begin::Entry-->
<div class="box">
    <div class="box-header">
            <div class="box-icon">
            
        </div>
    </div>
    <div class="box-content" di="printdiv">
        <div class="row">
            <div class="col-lg-12">

           <div class="print-only1 col-xs-12 text-center padding010">
                    <div class="brand-logo">
                    @if(get_setting('system_logo_white') != null)
                        <img  class="b-logo"src="{{ uploaded_asset(get_setting('system_logo_white')) }}" alt="{{$shipment->branch->name}}{{$shipment->branch->email}}" >
                        @else
                            <img src="{{ static_asset('assets/img/logo.svg') }}" class="b-logo">
                        @endif
                    <h3 class="company-name"> {{$shipment->branch->name}} , {{$shipment->branch->email}} ,{{$shipment->branch->responsible_mobile}} </h3>
                <h4  style="direction: rtl;">فاتورة ضريبية</h4>
                    <h4 class="company-name">Tax Invoice</h4>
            </div>
        </div>
      
  
           <div class="well well-sm">
                        <div class="col-md-8 padding010" style="float: left">
                            <div class="clearfix"></div>
                                <div class="table-responsive">
                                    <table class="table table-bordered print-table order-table">
                                    <tr>
                                        <td>Invoice Number:</td>
                                        <td> {{$shipment->code}} </td>
                                        <td class="text-right"> {{$shipment->code}} </td>
                                        <td style="direction: rtl;">رقم الفاتورة:</td>
                                    </tr>
                                    <tr>
                                    <tr>
                                     <td>Invoice Issue Date:</td>
                                     <td>{{ $shipment->created_at->format('Y-m-d') }}</td>
                                     <td>{{ $shipment->created_at->format('Y-m-d') }}</td>
                                     <td style="direction: rtl;">تاريخ إصدار الفاتورة:</td>
                                  </tr>
                                                                  
                                
                                </table>
                            </div>
                            
                       </div>
                <div class="col-md-4 padding010 " style="float: left">
                      <div class="clearfix"></div>
                        <div class="order_barcodes text-right">

                        <?php $t_p=0; ?>
                        @foreach(\App\PackageShipment::where('shipment_id',$shipment->id)->get() as $package)
                        
                        @foreach(\App\CatalogPackageShipment::with('catalog')->where('package_shipment_id',$package->id)->get() as $cps)
                       <?php  $t_p=$t_p+$cps->price;?>
                        @endforeach
                       
                        @endforeach    
                        
                       <?php 
                        
                         $inv_date = date("Y-m-d h:i:s", strtotime($shipment->created_at));
                         $vat_total =($t_p*$shipment->tax_amount)/100;
                         $grand_total =$t_p +$vat_total;
                         $vat_no=$shipment->branch->vat_no;
                         $seller = $shipment->client_address;
                         $result = chr(1) . chr( strlen($seller) ) . $seller;
                         $result.= chr(2) . chr( strlen($vat_no) ) .$vat_no;
                         $result.= chr(3) . chr( strlen($inv_date) ) . $inv_date;
                         $result.= chr(4) . chr( strlen($grand_total) ) . $grand_total;
                         $result.= chr(5) . chr( strlen($vat_total ) ) . $vat_total;
                         $newQr = base64_encode($result); ?>
                       
                       
                         {!! QrCode::size(140)->generate($newQr);!!}
                          
                       
                           
                     </div>
                     </div>
                     <div class="col-xs-12 padding010">
                         <div class="clearfix"></div>
                            <div class="table-responsive">
                                <table class="table table-bordered print-table order-table">
                                    <thead>
                                        <tr class="bg-dark">
                                            <th class="text-left" colspan="2">From:</th>
                                            <th class="text-right" colspan="2" style="direction: rtl;">من:</th>
                                            <th class="text-left" colspan="2">To:</th>
                                            <th class="text-right" colspan="2" style="direction: rtl;">ل:</th>
                                        </tr>
                                </thead>
                                <tbody>
                                 <tr >
                                     <td>Name:</td>
                                     <td colspan="2">{{$shipment->client_address}}</td>
                                     <td style="direction: rtl;">اسم:</td>
                                     <td>Name:</td>
                                     <td  colspan="2">{{$shipment->reciver_address}}</td>
                                     <td style="direction: rtl;">اسم:</td>
                                 </tr>
                                
                                 <tr >
                                     <td>Phone Number :</td>
                                     <td colspan="2">{{$shipment->client_phone}}</td>
                                     <td style="direction: rtl;">رقم التليفون:</td>
                                     <td>Phone Number :</td>
                                     <td  colspan="2">{{$shipment->client_phone}}</td>
                                     <td style="direction: rtl;">رقم التليفون:</td>
                                 </tr>
                                 <tr >
                                     <td>Address :</td>
                                     <td colspan="2">{{$shipment->client_address}}</td>
                                     <td style="direction: rtl;">تبوك:</td>
                                     <td>Address :</td>
                                     <td  colspan="2">{{$shipment->client_address}}</td>
                                     <td style="direction: rtl;">تبوك:</td>
                                 </tr>
                                 <tr >
                                     <td>Country :</td>
                                     <td colspan="2">@if(isset($shipment->from_country->name)){{$shipment->from_country->name}} @endif</td>
                                     <td style="direction: rtl;">دولة:</td>
                                     <td>Country :</td>
                                     <td  colspan="2">@if(isset($shipment->to_country->name)){{$shipment->to_country->name}} @endif</td>
                                     <td style="direction: rtl;">دولة:</td>
                                 </tr>
                                 <tr >
                                     <td>Region :</td>
                                     <td colspan="2">@if(isset($shipment->from_state->name)){{$shipment->from_state->name}} @endif</td>
                                     <td style="direction: rtl;">منطقة:</td>
                                     <td>Region :</td>
                                     <td  colspan="2">@if(isset($shipment->to_state->name)){{$shipment->to_state->name}} @endif</td>
                                     <td style="direction: rtl;">منطقة:</td>
                                 </tr>
                                 
                                
                                </tbody>
                             </table>
                         </div>  
                       </div>
                       
                       <div class="col-xs-12 padding010">
                         <div class="clearfix"></div>
                            <div class="table-responsive">
                               
                            <table class="table table-bordered table-striped table-condensed ">
                                   
                                <thead>
                                    <tr>
                                        <th class="text-center font-weight-bold text-muted text-uppercase">Item Name<br>اسم العنصر</th>
                                        <th class="text-center font-weight-bold text-muted text-uppercase">Quantity<br>كمية</th>
                                        <th class="text-center font-weight-bold text-muted text-uppercase">Weight(KG)<br>الوزن (كلغ)</th>
                                        <th class="text-center font-weight-bold text-muted text-uppercase">Taxable Amount<br>المبلغ الخاضع للضريبة</th>
                                        <th class="text-center font-weight-bold text-muted text-uppercase">Tax Rate(%)<br>معدل الضريبة(٪) </th>
                                        <th class="text-center font-weight-bold text-muted text-uppercase">Tax Amount<br>قيمة الضريبة</th>
                                        <th class="text-center font-weight-bold text-muted text-uppercase"> Item Subtotal (Including VAT)<br>المجموع الفرعي للعنصر (بما في ذلك ضريبة القيمة المضافة) </th>
                                       
                                    </tr>
                                    </thead>
                                    <tbody>
                                  <?php   $total_price =0; ?>
                                    @foreach(\App\PackageShipment::where('shipment_id',$shipment->id)->get() as $package)
                                   <tr> <td  class="text-left" colspan="7"><b>@if(isset($package->package->name)){{$package->package->name}} @else - @endif</b></td></tr>
                                   <?php $subtotal_price =0; ?>
                                   @foreach(\App\CatalogPackageShipment::with('catalog')->where('package_shipment_id',$package->id)->get() as $cps)
                                   <?php $total_price=$total_price+$cps->price ;$subtotal_price=$subtotal_price+$cps->price;
                                       
                                   ?>
                                    <tr> <td>{{$cps->catalog->name}}<br>{{$cps->catalog->other_lang}}</td>
                                         <td>{{$cps->qty}}</td>
                                         <td>{{$cps->wgt}}</td>
                                         <td>{{$cps->price}}</td>
                                         <td>{{$shipment->tax_amount}}</td>
                                         <td><?=$tax_amnt =($cps->price*$shipment->tax_amount)/100?>
                                         <td><?=$tax_amnt+$cps->price?></td>
                                         <!-- <td class="text-primary pr-0 pt-7 text-right align-middle">{{$package->weight." ".translate('KG')." x ".$package->length." ".translate('CM')." x ".$package->width." ".translate('CM')." x ".$package->height." ".translate('CM')}}</td> -->
                                    </tr>
                                    @endforeach
                                 
                                    @endforeach
                                    <tr>
                                    <td style="text-align:right;"colspan="5" class="font-weight-bold">Total (Excluding VAT) (USD)</td><td class="font-weight-bold">الإجمالي (باستثناء ضريبة القيمة المضافة) (USD)</td>
                                        <td class="font-weight-bold"><?=$total_price?></td>
                                    </tr>
                                    <tr>
                                    <td style="text-align:right;"colspan="5" class="font-weight-bold">Total VAT</td><td class="font-weight-bold">إجمالي ضريبة القيمة المضافة</td>
                                        <td class="font-weight-bold"><?=$total_vat=($total_price*$shipment->tax_amount)/100?></td>
                                    </tr>
                                    <tr>
                                    <td style="text-align:right;"colspan="5" class="font-weight-bold">Total Amount Due</td><td class="font-weight-bold">إجمالي المبلغ المستحق	</td>
                                        <td class="font-weight-bold"><?= $grand_total = $total_price +$total_vat?></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                <div class="clearfix"></div>

            </div>
        </div>
     
        
    </div>
</div>

@section('script')

<script>
window.onload = function() {
    $("#kt_footer").remove();
	javascript:window.print();
};
</script>
@endsection
