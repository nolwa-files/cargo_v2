<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CustomsShipment extends Model
{
    protected $guarded = [];


public function client(){
    return $this->belongsTo(Client::class);
}
public function country(){
    return $this->belongsTo(Countries::class);
}
}