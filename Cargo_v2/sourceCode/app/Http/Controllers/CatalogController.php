<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroyCatalogRequest;
use App\Models\Catalog;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class CatalogController extends Controller
{
    public function index()
    {
        $catalogs = Catalog::where('is_delete', 0)->paginate(15);
        return view('backend.catalogs.index', compact('catalogs'));
    }

    public function create()
    {
        return view('backend.catalogs.create');
    }

    public function store(Request $request)
    {
       $catalog = Catalog::create($request->all());
       return redirect()->route('admin.catalogs.index');
    }

    public function edit(Catalog $catalog)
    {
        return view('backend.catalogs.edit', compact('catalog'));
    }

    public function update(Request $request, Catalog $catalog)
    {
        $catalog->update($request->all());

        return redirect()->route('admin.catalogs.index');
    }

    public function show(Catalog $catalog)
    {
    }

    public function destroy(Catalog $catalog)
    {
        $catalog->delete();

        return back();
    }
}
