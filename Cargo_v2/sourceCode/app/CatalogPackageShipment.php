<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CatalogPackageShipment extends Model
{
    //
    protected $guarded = [];

    public function catalog()
    {
    return $this->belongsTo(Models\Catalog::class);
    }
}
